package config;

import models.User;
import org.springframework.batch.item.ItemProcessor;

public class UserItemProcessor implements ItemProcessor<User, User> {
    @Override
    public User process(User user) {
        if (user.getBalance() < 10) {
            System.out.println(user.getName() + " please add some credit, current balance: " + user.getBalance());
        }
        return user;
    }
}
